<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	</head>
	<body>
		<style>
			.btn-link {
				color: #ffffff !important;
				text-decoration: none;
			}
			.btn-link:hover {
				color: #ffffff !important;
				text-decoration: none;
			}

			.btn-primary {
				background-color:#9aabb8 !important;
				border: 0px !important;
				border-radius: 0px !important;
			}
			
			.btn-primary:hover {
				background-color: #e1b49a !important;
			}

			.card {
				margin-bottom: 5px !important;
			}

			.card-header {
				background-color:#9aabb8 !important;
				border: 0px !important;
				border-radius: 0px !important;
			}

			.card-header:hover {
				background-color: #e1b49a !important;
			}
		</style>
		<script>
		function steptwo(){
			document.getElementById("step_one").style.display = "none";
			document.getElementById("step_two").style.display = "block";
		}

		function stepthree(){
			document.getElementById("step_two").style.display = "none";
			// document.getElementById("step_three").style.display = "block";
			document.getElementById("step_four").style.display = "block";
		}

		function stepfour(){
			document.getElementById("step_three").style.display = "none";
			document.getElementById("step_four").style.display = "block";
		}

		</script>
		<div class="container">
			<form action="" method="POST" enctype="multipart/form-data">
				<div id="step_one">
					<h3>Get Started</h3>
					<p>Welcome to Letters To Rosie. Over the course of time you've been replying to our many questions via email. We'd love to provide you with a complimentary ebook showcasing those answers, and a special gift from us. If you're interested in receiving your free ebook follow the directions below to get started.</p><br/>
					<p>
						<button type="button" class="btn btn-primary" onClick="steptwo()">Continue</button>
					</p>
				</div>
				<div id="step_two" style="display:none;">
					<h3>Upload Photo</h3>
					<p>Provide us with a beautiful cover photo, this will be included in your ebook. The photo should focus on happy memories that remind you of the good times.</p>
					<br/><input type="file" name="photo" id="file_browse"/><br/><br/>
					<p>
						<button type="submit" class="btn btn-primary">Submit</button>
					</p>
				</div>
				<!--div id="step_three" style="display:none;">
					<h3>Your Answers</h3>
					<p>Select a date range of answers that you would like to include in your ebook.</p>
					<br/>From:<br/><br/>
					<select>
						<option>January</option>
						<option>Febuary</option>
						<option>March</option>
						<option>April</option>
						<option>May</option>
						<option>June</option>
						<option>July</option>
						<option>August</option>
						<option>September</option>
						<option>November</option>
						<option>December</option>
					</select>
					<br/><br/>To:<br/><br/>
					<select>
						<option>January</option>
						<option>Febuary</option>
						<option>March</option>
						<option>April</option>
						<option>May</option>
						<option>June</option>
						<option>July</option>
						<option>August</option>
						<option>September</option>
						<option>November</option>
						<option>December</option>
					</select><br/><br/>
					<p>
						<button type="button" class="btn btn-primary" onClick="stepfour()">Continue</button>
					</p>
				</div-->
				<div id="step_four" style="display:none;">
					<h3>Congrats!</h3>
					<p>Thank you so much! You will receive an email containing your personalized ebook.</p>
				</div>
			</form>
		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	</body>
</html>