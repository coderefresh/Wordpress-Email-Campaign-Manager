<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	</head>
	<body>
		<style>
			.btn-link {
				color: #ffffff !important;
				text-decoration: none;
			}
			.btn-link:hover {
				color: #ffffff !important;
				text-decoration: none;
			}

			.btn-primary {
				background-color:#9aabb8 !important;
				border: 0px !important;
				border-radius: 0px !important;
			}

			.btn-primary:hover {
				background-color: #e1b49a !important;
			}

			.card {
				margin-bottom: 5px !important;
			}

			.card-header {
				background-color:#9aabb8 !important;
				border: 0px !important;
				border-radius: 0px !important;
			}

			.card-header:hover {
				background-color: #e1b49a !important;
			}
		</style>
		<div class="container">
		@if ($updated)
		<div class="alert alert-success" role="alert">
		  Your answer has been updated!
		</div>
		@endif
		<div>
			<form action="" method="POST">				
				<input type="hidden" name="change_campaign" value="true"/>
				<select name="campaign_id" class="form-control">
					@foreach($campaigns as $campaign)
						<option value="{{ $campaign['campaign']->id }}">Child: {{ $campaign['child']->first_name }} - Campaign: {{ $campaign['campaign']->name }}</option>
					@endforeach
				</select>
				<button class="btn btn-primary" style="float:right; margin-bottom: 10px; margin-top: 10px;" type="submit">View Campaign Answers</button>
			</form>
		</div><br/><br/><br/>
		@forelse($answers as $answer)
		<div class="accordion" id="accordion{{ $answer['idName'] }}">
		  
		  <div class="card">
		    <div class="card-header" id="heading{{ $answer['idName'] }}" style="background-color:#9aabb8;">
		      <h5 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $answer['idName'] }}" aria-expanded="true" aria-controls="collapse{{ $answer['idName'] }}">
		          {{ $answer['answer']->question->question }} - {{ date('m/d/Y h:i A', strtotime($answer['answer']->question->updated_at)) }}
		        </button>
		      </h5>
		    </div>

		    <div id="collapse{{ $answer['idName'] }}" class="collapse" aria-labelledby="heading{{ $answer['idName'] }}" data-parent="#accordion{{ $answer['idName'] }}">
		      <div class="card-body">
		      	<form action="" method="POST">
		      		<input type="hidden" name="update_answer" value="true"/>
		      		<input type="hidden" name="answer_id" value="{{ $answer['answer']->id }}"/>
		        	<textarea name="answer" style="width: 100%;border: 1px solid rgba(0,0,0,.1);min-height: 200px;">{{ $answer['answer']->answers }}</textarea><br/><br/>
		        	<button class="btn btn-primary" style="float:right; margin-bottom: 10px; margin-top: 10px;" type="submit">Update</button>

		    	</form>

		      </div>
		    </div>
		  </div>
		  
		</div>
		@empty
    		<h2>No Letters</h2>
    		<p>You have not responded to any letters yet. You will receive weekly emails asking you questions, once you respond to those you will begin seeing letters appear here.</p>
		@endforelse
		</div>

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	</body>
</html>