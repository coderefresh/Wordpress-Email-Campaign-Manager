<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Campaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // if ((DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') && version_compare(DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge')) {
            //     $table->json('questions');
            // } else {
            //     $table->text('questions');
            // }
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('email_campaigns_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->timestamps();
        });

        Schema::create('children', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nickname');
            $table->string('dob');
            $table->string('salutation');
            $table->string('parent_nickname');
            $table->string('parent_role');
            $table->string('parent_signature');
            $table->timestamps();
        });

        //Pivot table
        Schema::create('campaign_question', function (Blueprint $table) {
            $table->integer('campaign_id')->unsigned()->index();
            $table->foreign('campaign_id')->references('id')->on('email_campaigns')->onDelete('cascade');
            $table->integer('question_id')->unsigned()->index();
            $table->foreign('question_id')->references('id')->on('email_campaigns_questions')->onDelete('cascade');
            $table->primary(['campaign_id', 'question_id']);
        });

        Schema::create('email_campaigns_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->integer('campaign_id');
            $table->integer('question_id');
            $table->integer('child');
            // $table->foreign('campaign_id')->references('id')->on('email_campaigns')->onDelete('cascade');
            $table->text('answers');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_campaigns');
        Schema::dropIfExists('email_campaigns_questions');
        Schema::dropIfExists('email_campaigns_answers');
    }
}
