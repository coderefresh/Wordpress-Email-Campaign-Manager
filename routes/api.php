<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
//use \Woocommerce;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::match(['get','post'],'/webhooks/order_created', 'Admin\CampaignsCrudController@orderCreated');
Route::get('/webhooks/order_created_fixed', 'Admin\CampaignsCrudController@orderCreated');
Route::post('/webhooks/inbound_email', 'Admin\CampaignsCrudController@inboundEmail');
Route::get('/cron/emails', 'Admin\CampaignsCrudController@sendEmails'); //Scheduled for every Monday
Route::match(['get','post'], '/letters/{user_email}', 'Admin\CampaignsCrudController@letters');
Route::match(['get','post'],'/ebook/{user_email}', 'Admin\CampaignsCrudController@ebook');