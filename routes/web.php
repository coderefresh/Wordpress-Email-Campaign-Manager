<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pdf/{answer_id}', 'Admin\CampaignsCrudController@displayAnswers');

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [ ],
    'namespace'  => 'Admin',
], function () { // custom admin routes
	CRUD::resource('answers', 'AnswersCrudController');
	CRUD::resource('campaigns', 'CampaignsCrudController');
	CRUD::resource('questions', 'QuestionsCrudController');

}); 
