<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CampaignsRequest as StoreRequest;
use App\Http\Requests\CampaignsRequest as UpdateRequest;

/* Import Classes */
use App\Models\Answers;
use App\Models\Questions;
use App\Models\Campaigns;

use Auth;

class QuestionsCrudController extends CrudController
{
    public function setup()
    {

        $this->middleware('guest');
        Auth::loginUsingId(1); //Auto auth user.

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Questions');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/questions');
        $this->crud->setEntityNameStrings('Question', 'Questions');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
