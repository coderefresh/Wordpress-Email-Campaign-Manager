<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CampaignsRequest as StoreRequest;
use App\Http\Requests\CampaignsRequest as UpdateRequest;

/* Import Classes */
use App\Models\Answers;
use App\Models\Questions;
use App\Models\Campaigns;
use App\User;

use Postmark\PostmarkClient;
use Postmark\Models\PostmarkAttachment;

use Woocommerce;
use Auth;
use App\Models\Child;

class CampaignsCrudController extends CrudController
{
    public function setup()
    {
        $this->middleware('guest');
        Auth::loginUsingId(1); //Auto auth user.

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Campaigns');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/campaigns');
        $this->crud->setEntityNameStrings('campaigns', 'campaigns');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addColumn(
            [ 
                'name' => 'active',
                'label' => 'Active',
                'type' => 'boolean',
                'options' => [0 => 'Inactive', 1 => 'Active']
            ]
        );

        $this->crud->addColumn(
            [ 
                'name' => 'created_at',
                'label' => 'Created',
                'type' => 'datetime',
                'format' => 'm/d/Y h:i A'
            ]
        );

        $this->crud->addField(
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => "Questions",
                'type' => 'select2_multiple',
                'name' => 'questions', // the method that defines the relationship in your Model
                'entity' => 'questions', // the method that defines the relationship in your Model
                'attribute' => 'question', // foreign key attribute that is shown to user
                'model' => "App\Models\Questions", // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'select_all' => false, // show Select All and Clear buttons?
                
            ]
        );

        $this->crud->addField(
            [  // Select
                'name' => 'active',
                'label' => 'Active',
                'type' => 'checkbox'
            ]
        );
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function orderCreated(Request $request){
        //GRAB LATEST ORDER
        $orders         = Woocommerce::get('orders');
        $latestOrder    = $orders[0]; //Modified since it's already ordered by date.
        $product        = $latestOrder['line_items'][0];

        $child = array(
            'first_name' => '',
            'last_name' => '',
            'nickname' => '',
            'dob' => '',
            'salutation' => ''
        );

        $parent = array(
            'nickname' => '',
            'role' => '',
            'signature' => ''
        );

        foreach ($product['meta_data'] as $key => $value) {
            if ($value['key'] == 'child_s_first_name'){
                $child['first_name'] = $value['value'];
            }else if ($value['key'] == 'child_s_last_name'){
                $child['last_name'] = $value['value'];
            }else if ($value['key'] == 'child_s_nickname'){
                $child['nickname'] = $value['value'];
            }else if ($value['key'] == 'child_s_date_of_birth'){
                $child['dob'] = $value['value'];
            }else if ($value['key'] == 'nickname_for_you'){
                $parent['nickname'] = $value['value'];
            }else if ($value['key'] == 'select_role'){
                $parent['role'] = $value['value'];
            }else if ($value['key'] == 'salutation_to_child'){
                $child['salutation'] = $value['value'];
            }else if ($value['key'] == 'signature'){
                $parent['signature'] = $value['value'];
            }
        }

        if (is_null($child) || is_null($parent)) { return; }



        $customer       = Woocommerce::get('customers/'.$latestOrder['customer_id']);
        $email          = $customer['email'];

        $this->sendWelcomeEmail($email, $product['name']);

        if (User::where('email','=',$email)->exists() == false){
            User::create([
                "name" => $customer['first_name'] . ' ' . $customer['last_name'],
                "email" => $email,
                "password" => ""
            ]);            
        }

        $campaign = Campaigns::where('active', '=', 1)->first();
        $campaign->assignBlankAnswers($email, $child, $parent);



        //CRON for every Monday
        //DISABLE WHEN READY
        $this->sendEmail($request, $email);
    }

    public function sendWelcomeEmail($email, $product_name){
        $user           = User::where('email','=',$email)->first();
        $template_id    = config('POSTMARK_TEMPLATE', '7706998');
        $fromEmail      = config('POSTMARK_SENDER', 'hello@letterstorosie.com');
        $postmark_token = config('POSTMARK_TOKEN', 'bc879c86-e5d4-44a6-9d8f-783684aa7fb3');
        $reply_to       = config('POSTMARK_REPLYTO', '0a1e42f42d55b9bf58d5c7dd39b577d9@inbound.postmarkapp.com');

        try{
            $client = new PostmarkClient($postmark_token);

            $sendResult = $client->sendEmailWithTemplate(
              $fromEmail,
              $email, 
              $template_id, 
              [
                "first_name" => explode(' ', $user->name)[0],
                "product_name" => $product_name,
              ],
              true,
              NULL,
              true,
              $reply_to
            );
        }catch(PostmarkException $ex){
            // If client is able to communicate with the API in a timely fashion,
            // but the message data is invalid, or there's a server error,
            // a PostmarkException can be thrown.
            echo $ex->httpStatusCode;
            echo $ex->message;
            echo $ex->postmarkApiErrorCode;

        }catch(Exception $generalException){
            // A general exception is thrown if the API
            // was unreachable or times out.
            // dd($generalException);
        }

        return json_encode(array("success" => true));
        
    }

    public function sendEmails(Request $request){
        $users = User::all();
        $campaign = Campaigns::where('active', '=', 1)->first();
        foreach ($users as $user_key => $user) {
            $campaign->assignBlankAnswers($user->email);
            $this->sendEmail($request, $user->email);
        }
    }

    public function sendEmail(Request $request, $email = ""){
        $user           = User::where('email','=',$email)->first();
        $campaign       = Campaigns::where('active', '=', 1)->first();
        $questions      = $campaign->questions;
        $curQuestion    = $campaign->getCurrentQuestion($email);

        if ($curQuestion == false){
            //End of campaign;
            return;
        }

        $question_id    = $curQuestion['id'];
        $curQuestion    = $curQuestion['question'];

        $child_id       = Answers::where([['email','=',$email],['question_id','=',$question_id]])->first()->child;
        $child          = Child::where('id','=',$child_id)->first();

        $name           = $user->name;

        $template_id    = config('POSTMARK_TEMPLATE', '7260861');
        $fromEmail      = config('POSTMARK_SENDER', 'hello@letterstorosie.com');
        $postmark_token = config('POSTMARK_TOKEN', 'bc879c86-e5d4-44a6-9d8f-783684aa7fb3');
        $reply_to       = config('POSTMARK_REPLYTO', '0a1e42f42d55b9bf58d5c7dd39b577d9@inbound.postmarkapp.com');

        /**
         * Old Template Variables
         *[
            "name" => $name,
            "campaign_id" => $campaign->id,
            "campaign_name" => $campaign->name,
            "product_name" => $product,
            "questions" => $campaignQuestions
         *]
        */

        if (!$campaign->hasAnsweredCampaign($email)){
            try{
                $client = new PostmarkClient($postmark_token);

                $sendResult = $client->sendEmailWithTemplate(
                  $fromEmail,
                  $email, 
                  $template_id, 
                  [
                    "first_name" => explode(' ', $user->name)[0],
                    "question" => $curQuestion,
                    "campaign_id" => $campaign->id,
                    "question_id" => $question_id,
                    "child_first_name" => $child->first_name,
                    "parent_nickname" => $child->parent_nickname
                  ],
                  true,
                  NULL,
                  true,
                  $reply_to
                );
            }catch(PostmarkException $ex){
                // If client is able to communicate with the API in a timely fashion,
                // but the message data is invalid, or there's a server error,
                // a PostmarkException can be thrown.
                echo $ex->httpStatusCode;
                echo $ex->message;
                echo $ex->postmarkApiErrorCode;

            }catch(Exception $generalException){
                // A general exception is thrown if the API
                // was unreachable or times out.
                // dd($generalException);
            }

            return json_encode(array("success" => true));
        }
    }

    public function inboundEmail(Request $request){
        $object = json_decode($request->getContent(), true);
        
        $fromEmail      = $object['From'];
        $subject        = $object['Subject'];

        $campaign_and_question_id    = $this->retrieveCampaignIdFromSubject($subject);
        $campaign_id = $campaign_and_question_id['campaign_id'];
        $question_id = $campaign_and_question_id['question_id'];

        if ($campaign_id == -1){
            return json_encode(array("success" => false, "error" => "NO CAMPAIGN_ID FOUND."));
        }

        $queryAnswers   = Answers::where([['email', '=', $fromEmail], ['campaign_id', '=', $campaign_id]]);

        if ($queryAnswers->exists()){
            $answers = $queryAnswers->get();
            foreach ($answers as $answer_key => $answer) {
                if ($answer->question_id == $question_id){
                    $answer->answers = $object['StrippedTextReply'];
                    $answer->status = 2;
                    $answer->save();
                }
            }

            return json_encode(array("success" => true));
        }else{
            return json_encode(array("success" => false, "error" => "NO ANSWER RECORD EXISTS."));
        }
    }

    public function sendAnswersEmail($email, $url){
        $user           = User::where('email','=',$email)->first();

        $template_id    = config('POSTMARK_TEMPLATE_2', '7314981');
        $fromEmail      = config('POSTMARK_SENDER', 'hello@letterstorosie.com');
        $postmark_token = config('POSTMARK_TOKEN', 'bc879c86-e5d4-44a6-9d8f-783684aa7fb3');
        $reply_to       = config('POSTMARK_NOREPLY', 'noreply@letterstorosie.com');

        try{
            $client = new PostmarkClient($postmark_token);

            $sendResult = $client->sendEmailWithTemplate(
              $fromEmail,
              $email, 
              $template_id, 
              [
                "first_name" => explode(' ', $user->name)[0],
                "ebook_url" => $url
              ],
              true,
              NULL,
              true,
              $reply_to
            );
        }catch(PostmarkException $ex){
            // If client is able to communicate with the API in a timely fashion,
            // but the message data is invalid, or there's a server error,
            // a PostmarkException can be thrown.
            echo $ex->httpStatusCode;
            echo $ex->message;
            echo $ex->postmarkApiErrorCode;

        }catch(Exception $generalException){
            // A general exception is thrown if the API
            // was unreachable or times out.
        }

        return json_encode(array("success" => true));
    }

    public function sendReminderEmail(Request $request){

    }

    public function generateEbook(Request $request, $email){
        $imgextension = $request->file('photo')->extension();
        $image = base64_encode(file_get_contents($request->file('photo')->path()));
        $imageBase64 = 'data:image/' . $imgextension . ';base64,' . ($image);

        $answers            = Answers::where([['email','=',$email], ['status','=',2]])->get();
        $contents           = file_get_contents(public_path().'/templates/template.html');
        $allQuestionsHtml   = "";

        foreach ($answers as $answer_key => $answer) {
            $question       = Questions::where('id','=',$answer->question_id)->first();
            $questionFormat = '
                <div class="question_holder">
                  <div class="question">
                    <h3>%QUESTION%</h3>
                  </div>
                  <div class="answer">
                    <p>%ANSWER%</p>
                  </div>
                </div>';

            $questionFormat     = str_replace("%QUESTION%", $question->question, $questionFormat);
            $questionFormat     = str_replace("%ANSWER%", $answer->answers, $questionFormat);
            $allQuestionsHtml   .= $questionFormat;
        }

        $contents   = str_replace("%QUESTIONS%", $allQuestionsHtml, $contents);
        $contents   = str_replace("%IMAGE%", $imageBase64, $contents);
        $pdf        = \App::make('dompdf.wrapper');
        $pdf->loadHTML($contents);
    
        $email = urldecode($email);
        $email = str_replace('.', '', $email); //Extension hotfix.
        $email = str_replace('@', '', $email); //Extension hotfix.

        $pdf->save('../storage/app/public/'.($email).'.pdf');

        return url('storage/'.$email.'.pdf');
    }

    public function retrieveCampaignIdFromSubject($subject){
        $matches = array();
        preg_match("/\(#(.*)\)$/", $subject, $matches);
        if (sizeof($matches) == 0){
            return -1;
        }else{
            $match = $matches[1];
            $campaign_id = explode('-', $match)[0];
            $question_id = explode('-', $match)[1];
            return array("campaign_id" => $campaign_id, "question_id" => $question_id);
        }
    }

    public function letters(Request $request, $user_email){
        $updated = false;
        $campaign_id = false;

        if ($request->isMethod('post')){
            //Update
            if ($request->has('answer_id') && $request->has('update_answer') && $request->has('answer')){
                $update_answer = $request->input('update_answer');
                $answer_id = $request->input('answer_id');
                $answer = $request->input('answer');

                $answerQuery = Answers::where('id','=',$answer_id);

                if ($answerQuery->exists()){
                    $answerQuery = $answerQuery->first();
                    $answerQuery->answers = $answer;
                    $answerQuery->save();
                    $updated = true;
                }
            }else{
                $updated = false;
            }

            //Change Campaign
            if ($request->has('change_campaign') && $request->has('campaign_id')){
                if (Campaigns::where('id','=',$request->input('campaign_id'))->exists()){
                    $campaign_id = intval($request->input('campaign_id'));
                }else{
                    $campaign_id = false;
                }
            }
        }

        $campaigns = Campaigns::all();
        $user_email = urldecode($user_email);
        $answers = Answers::where('email', '=', $user_email)->get();

        $finalCampaigns = array();

        $_ids = array();
        foreach ($answers as $answer_key => $answer_value) {
            if ($campaign_id != false){
                if ($answer_value->campaign_id != $campaign_id){
                    continue;
                }
            }

            $finalCampaigns = array_merge($finalCampaigns, array(array('child' => $answer_value->child, 'campaign' => $answer_value->campaign_id)));

            $_ids = array_merge($_ids, array(array("idName" => $answer_value->generateRandomString(), "answer" => $answer_value)));
        }

        $anotherFinalCampaigns = array();
        foreach ($finalCampaigns as $_campaign_key => $_campaign) {
            $child = Child::where('id','=',$_campaign['child'])->first();
            $campaign = Campaigns::where('id','=',$_campaign['campaign'])->first();
            $anotherFinalCampaigns = array_merge($anotherFinalCampaigns, array(array('campaign'=>$campaign, 'child' => $child)));
        }


        return view('letters', ['answers' => $_ids, 'campaigns' => $anotherFinalCampaigns, 'updated' => $updated]);
    }

    public function ebook(Request $request, $user_email){
        $user_email = urldecode($user_email);
            
        if ($request->isMethod('post')) {
            $ebookUrl = $this->generateEbook($request, $user_email);
            $this->sendAnswersEmail($user_email, $ebookUrl);
            return view('ebook_completed');
        }else{
            return view('ebook');
        }
    }
}
