<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AnswersRequest as StoreRequest;
use App\Http\Requests\AnswersRequest as UpdateRequest;

/* Import Classes */
use App\Models\Answers;
use App\Models\Questions;
use App\Models\Campaigns;
use App\User;

use Postmark\PostmarkClient;
use Postmark\Models\PostmarkAttachment;

use Woocommerce;
use Auth;

class AnswersCrudController extends CrudController
{
    public function setup()
    {
        $this->middleware('guest');
        Auth::loginUsingId(1); //Auto auth user.

        $this->crud->enableExportButtons();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Answers');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/answers');
        $this->crud->setEntityNameStrings('answers', 'answers');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn(
            [ 
                'name' => 'name',
                'label' => 'Name',
                'type' => 'model_function',
                'function_name' => 'getName'
            ]);
        $this->crud->addColumn(
            [ 
                'name' => 'phone',
                'label' => 'Phone',
                'type' => 'model_function',
                'function_name' => 'getPhone'
            ]);
        $this->crud->addColumn(
            [ 
                'name' => 'email',
                'label' => 'Email'
            ]);
        $this->crud->addColumn(
            [ 
                'name' => 'campaign',
                'label' => 'Campaign',
                'type' => 'model_function',
                'function_name' => 'getCampaignName'
            ]);
        $this->crud->addColumn(
            [ 
                'name' => 'question',
                'label' => 'Question',
                'type' => 'model_function',
                'function_name' => 'getQuestion'
            ]);
        $this->crud->addColumn(
            [ 
                'name' => 'answers',
                'label' => 'Answer',
                'type' => 'model_function',
                'function_name' => 'getAnswer'
            ]);

        $this->crud->denyAccess(['create', 'update', 'delete']);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
