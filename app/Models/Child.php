<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\User;
use App\Models\Questions;
use App\Models\Answers;

class Child extends Model
{
    //use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'children';
    protected $guarded = ['id'];
    protected $fillable = ['first_name', 'last_name', 'nickname', 'dob', 'salutation', 'parent_nickname', 'parent_role', 'parent_signature'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    // public function campaigns(){
    //     return $this->belongsToMany('App\Models\Questions', 'campaign_question', 'campaign_id', 'question_id');
    //     //return $this->hasMany('App\Models\Questions', 'campaign_id', 'id');
    // }

    // public function answers(){
    //     return $this->hasMany('App\Models\Answers', 'campaign_id', 'id');
    // }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
