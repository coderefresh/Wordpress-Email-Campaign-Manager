<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use App\Models\Answers;
use App\Models\Questions;
use App\Models\Campaigns;

class Questions extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'email_campaigns_questions';
    protected $guarded = ['id'];
    protected $fillable = ['question'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getSlugForCampaign(){
        $campaign_id = $this->campaign_id;
        $campaign = Campaigns::where('id','=',$campaign_id)->first();
        $url = url('manager/campaigns/'.$campaign_id.'/edit');
        return $campaign->name;
        // return '<a href="'.$url.'">'.$campaign->name.'</a>';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function campaigns(){
        return $this->belongsToMany('App\Models\Campaigns', 'campaign_question', 'id', 'question_id');
    }

    public function campaign(){
        return $this->hasOne('App\Models\Campaigns', 'id', 'campaign_id');
    }

    public function answers(){
        return $this->hasMany('App\Models\Answers', 'question_id', 'id');
    }

    public function hasAnsweredQuestion($email){
        $answers = $this->answers;
        foreach ($answers as $answer) {
            if ($answer->email == $email){
                return true;
            }
        }

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
