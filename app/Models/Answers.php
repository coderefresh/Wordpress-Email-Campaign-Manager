<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/* Import Classes */
use App\Models\Questions;
use App\Models\Campaigns;
use App\User;

use Postmark\PostmarkClient;
use Postmark\Models\PostmarkAttachment;

use Woocommerce;
use Auth;

class Answers extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'email_campaigns_answers';
    protected $guarded = ['id'];
    protected $fillable = ['email', 'campaign_id', 'question_id', 'child', 'answers', 'status'];
    protected $fakeColumns = ['name', 'phone']; 

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function generateRandomString() {
        $length = 5;
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getQuestion(){
        return $this->question->question;
    }

    public function getName(){
        $user = $this->getUser();
        if (is_null($user)){
            return '';
        }

        return $user['first_name'] . ' ' . $user['last_name'];
    }

    public function getPhone(){
        $user = $this->getUser();
        if (is_null($user)){
            return '';
        }

        return $user['phone'];
    }

    public function getCampaignName(){
        return $this->campaign->name;
    }

    public function getAnswer(){
        return ($this->answers != '.') ? $this->answers : '';
    }

    public function getUser(){
        $users = Woocommerce::get('customers');
        foreach ($users as $user_key => $user) {
            if ($user['billing']['email'] == $this->email){
                return $user['billing'];
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function campaign(){
        return $this->hasOne('App\Models\Campaigns', 'id', 'campaign_id');
    }

    public function question(){
        return $this->hasOne('App\Models\Questions', 'id', 'question_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
