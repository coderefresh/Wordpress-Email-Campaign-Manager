<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\User;
use App\Models\Questions;
use App\Models\Answers;
use App\Models\Child;

class Campaigns extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'email_campaigns';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'questions', 'active'];
    protected $casts = [
        'questions' => 'array',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function hasAnsweredCampaign($email){
        return false; //@TODO handle hasAnswerCampaign method.
    }

    public function getCurrentQuestion($email){
        $user = User::where('email','=',$email);
        if ($user->exists()){
            $user = $user->first();

            $answers = $this->answers()->where('email','=',$email)->orderBy('id','asc')->get();
            foreach ($answers as $answer_key => $answer) {
                if ($answer->answers == "."){
                    $question = Questions::where('id', '=', $answer->question_id)->first()->question;
                    $answer->status = 1;
                    $answer->save();

                    return array('id' => $answer->question_id, 'question' => $question);
                }
            }

            return false;
        }
    }

    public function assignBlankAnswers($email, $child, $parent){
        $user = User::where('email','=',$email);
        if ($user->exists()){
            $questions = $this->questions;
            foreach ($questions as $question_key => $question) {
                $this->assignQuestion($email, $question->id, $this->id, $child, $parent);
            }
        }
    }

    public function assignQuestion($email, $question_id, $campaign_id, $child, $parent){
        $answers = Answers::where('email','=',$email)->get();
        $exists = false;
        foreach ($answers as $answer_key => $answer) {
            if ($answer->campaign_id == $campaign_id && $answer->question_id == $question_id){
                $exists = true;
            }
        }

        if (!$exists){
            if (is_null($child) || is_null($parent)){ return false; }

            $child = Child::create([
                'first_name' => $child['first_name'], 
                'last_name' => $child['last_name'], 
                'nickname' => $child['nickname'], 
                'dob' => $child['dob'], 
                'salutation' => $child['salutation'], 
                'parent_nickname' => $parent['nickname'], 
                'parent_role' => $parent['role'], 
                'parent_signature' => $parent['signature']
            ]);

            Answers::create([
                'email' => $email,
                'campaign_id' => $campaign_id,
                'question_id' => $question_id,
                'child' => $child->id,
                'answers' => ".",
                'status' => 0
            ]);
        }

        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function questions(){
        return $this->belongsToMany('App\Models\Questions', 'campaign_question', 'campaign_id', 'question_id');
        //return $this->hasMany('App\Models\Questions', 'campaign_id', 'id');
    }

    public function answers(){
        return $this->hasMany('App\Models\Answers', 'campaign_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
