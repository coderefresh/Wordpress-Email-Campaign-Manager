<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Campaigns;
use App\Models\Answers;
use App\Models\Questions;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function assignQuestion($question_id, $campaign_id){
        $answers = Answers::where('email','=',$this->email)->all();
        $exists = false;
        foreach ($answers as $answer_key => $answer) {
            if ($answer->campaign_id == $campaign_id && $answer->question_id == $question_id){
                $exists = true;
            }
        }

        if (!$exists){
            Answers::create([
                'email' => $this->email,
                'campaign_id' => $campaign_id,
                'question_id' => $question_id,
                'answers' => '',
                'status' => 0
            ]);
        }

        return true;
    }
}
